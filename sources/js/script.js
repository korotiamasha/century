import Swiper from 'swiper/swiper-bundle';

(function ($) {
    const swiperServices = new Swiper('.services-swiper', {
        slidesPerView: 1,
        grid: {
            rows: 1,
        },
        spaceBetween: 20,
        navigation: {
            nextEl: '.services__next',
            prevEl: '.services__prev',
        },
        pagination: {
            el: '.services__pagination',
            type: 'bullets',
            clickable: true,
        },
        breakpoints: {
            767: {
                slidesPerView: 2,
            },

            991: {
                slidesPerView: 2,
                grid: {
                    rows: 2,
                },
            },
            1200: {
                slidesPerView: 3,
                grid: {
                    rows: 2,
                },
            }
        }
    });

    const swiperReviews = new Swiper('.reviews-swiper', {
        slidesPerView: 1,
        spaceBetween: 20,
        loop: true,

        breakpoints: {
            767: {
                slidesPerView: 2,
            }
        },
        navigation: {
            nextEl: '.reviews-next',
            prevEl: '.reviews-prev',
        },
        pagination: {
            el: '.reviews-pagination',
            type: 'bullets',
            clickable: true,
        },
    });

    /*header*/
    $('.header__burger').on('click', function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $('html').removeClass('modal-open');
            $('.main-nav').removeClass('active');
            $('.header__logo').removeClass('opacity-0');
        } else {
            $(this).addClass('active');
            $('html').addClass('modal-open');
            $('.main-nav').addClass('active');
            $('.header__logo').addClass('opacity-0');
        }
    });

    $(window).scroll(function () {
        let height = $('.main-banner').height(),
            scrollTop = $(window).scrollTop();

        if ($('.home-header').length > 0) {
            if (scrollTop >= height) {
                $('.header').addClass('fixed');
            } else  {
                $('.header').removeClass('fixed');
            }
        }
    });


    /*team*/
    let teamList = [
        {
            id: "team1",
            photo: "assets/img/team/team1.jpg",
            name: "Ирина Шилова",
            position: "Руководитель агентства",
            info: "<p>За 15 лет работы в продажах заключила более сотни контрактов</p><p>Более 5 лет опыта визуальной упаковки бизнеса для предпринимателей</p><p>Соавтор курса по продвижению и упаковке бьюти-бизнеса онлайн. Сотни благодарных отзывов. Результат учениц: от мастера на дому до владельца салона/сети салонов</p><p>Более 7 лет опыта в маркетинге",
        },
        {
            id: "team2",
            photo: "assets/img/team/team2.jpg",
            name: "Яна Бойкова",
            position: "Арт-директор",
            info: "<p>Стаж работы в графическом дизайне более 8 лет</p><p>Несколько лет делилась практическими знаниями в качестве ментора (тьютора) в школе графического дизайна</p><p>Работает в SMM-сфере более 5 лет. Большой опыт в продвижении как действующего бизнеса, так и стартапов</p><p>Сотрудничает в качестве SMM-специалиста/дизайнера с крупными игроками международного и отечественного рынка (Канада и США, Европа и страны СНГУ</p><p>Многократный победитель престижных конкурсов в сфере дизайна на зарубежных платформах</p> <p>Постоянно повышает квалификацию, участвуя в самых актуальных дизайн-конференциях и коллаборациях</p>",
        },
        {
            id: "team4",
            photo: "assets/img/team/team4.jpg",
            name: "Михаил Филичев",
            position: "Руководитель IT-отдела",
            info: "<p>Фанат компьютеров и программирования с детства. Он написал свою первую программу в возрасте 10 лет. Может собрать компьютер с закрытыми глазами)</p><p>Занимается созданием сайтов уже более 7 лет. За это время он создал более 200 веб-сайтов для различных сфер бизнеса</p><p>Может создавать веб-сайты любой сложности, от простых лендингов до сложных веб-сервисов. Использует в своей работе новейшие технологии</p><p>Постоянно учится и постигает новые горизонты своей профессии</p><p>Имеет ответ на любой из ваших вопросов о мире интернет-реальности</p><p>Он может решить практически любую поставленную перед ним задачу</p>",
        },
        {
            id: "team5",
            photo: "assets/img/team/team5.jpg",
            name: "Инна Елисеева",
            position: "Финансовый директор",
            info: "<p>Гуру недвижимости с 18-летним стажем в финансах и инвестициях</p><p>За спиной лидера рынка с управленческим складом ума несколько агентств по аренде жилья и опыт работы в самых различных сферах бизнеса</p><p>Консультирует и помогает всем, кому нужен результат. Инвестирует в тех, кто не боится играть и выигрывать</p>",
        },
        {
            id: "team7",
            photo: "assets/img/team/team7.jpg",
            name: "Степан Шапошников",
            position: "Руководитель отдела продаж",
            info: "<p>Уникальные познания и навыки в продажах и психологии</p><p>Постоянно учится и совершенствует опыт в маркетинге</p><p>Переехал в Лос-Анджелес, чтобы стать лучшим в сфере продаж</p><p>Профессиональный девиз «Я служу вашим интересам»</p>",
        },
        {
            id: "team8",
            photo: "assets/img/team/team8.jpg",
            name: "Юлия Иванова",
            position: "HR специалист",
            info: "<p>Более 13 лет опыта в HR. Помимо этого более 10 лет стажа в бюджетировании, 5 лет в стратегии и внутриорганизационном развитии, 8 лет в автоматизации бизнес процессов и 4 года в консалтинге</p><p>Закончила Университет Управления на факультете \"Национальная и мировая экономика\" по специализации малого бизнесаж; МВА \"Деловое администрирование и управление бизнесом\", \"Управление персоналом\"</p><p>Запустила более 50 стартапов в РФ, Европе и США</p>",
        },
        {
            id: "team9",
            photo: "assets/img/team/team9.jpg",
            name: "Александр Бондаренко",
            position: "Руководитель отдела продаж",
            info: "<p>Имеет стаж результативной работы в сфере продаж в лучших международных компаниях более 5 лет</p><p>Кроме развития федеральных и локальных сетей, имеет эффективный опыт руководства большой командой мерчендайзеров, торговых агентов и представителей</p><p>Во время сотрудничества с брендом Nestle пополнил не только базу клиентов бренда, но и свой внушительный список связей.</p><p>Главной победой в насыщенной карьере Александра стало привлечение и развитие крупных корпораций России и зарубежья, благодаря построению качественной дистрибуции</p>",
        },
        {
            id: "team10",
            photo: "assets/img/team/team10.jpg",
            name: "Ольга Федорова",
            position: "Юридическая поддержка",
            info: "<p>Юрист высшей категории с многолетним опытом на руководящих должностях.Более 10 лет успешно работает с лидерами малого и среднего бизнеса</p><p>Последние несколько лет ведет частную практику и регулярно повышает квалификацию с помощью признанных на международном уровне курсов и тренингов</p><p>Специалист-переводчик в деловой коммуникации со знанием английского языка на уровне native имеет обширную практику в сферах строительства и недвижимости, а также интеллектуальной собственности</p><p>Большую часть своей карьеры посвятила IT-компаниям и выстраиванию прибыльных бизнес-процессов</p><p>Опыт в регистрации товарных знаков и познания в защите коммерческой тайны и интеллектуальной собственности особенно пригодились во время коллаборации со начинающими предпринимателями</p>",
        },
    ];

    $('.js-modal-team').on('click', function () {
        let itemTeam = $(this).attr('data-modal');

        let item = teamList.find(o => o.id === itemTeam);

        //change info
        $('.js-modal-name').html(item.name);
        $('.js-modal-position').html(item.position);
        $('.js-modal-image').attr('src', item.photo);
        $('.js-modal-info').html(item.info);

        $('html').addClass('modal-open');
        $('.modal-team').addClass('show');
    });

    $('.js-modal-close').on('click', function () {
        $('.modal-team').removeClass('show');
        $('html').removeClass('modal-open');
    });

    $('.about-tab__item').on('click', function () {
        let tab = $(this).attr('data-tab');
        if (!($(this).hasClass('active'))) {
            $('.about-tab__item.active').removeClass('active');
            $('.about-tab__content.active').removeClass('active');
            $(this).addClass('active');
            $('#' + tab).addClass('active');
        }
    })

    /*end team*/


    /*tabs*/
    $('.tab__link').on('click', function () {
        let tab = $(this).attr('data-tab');
        if (!($(this).hasClass('active'))) {
            $('.tab__link.active').removeClass('active');
            $('.tab__content.active').removeClass('active');
            $(this).addClass('active');
            $('#' + tab).addClass('active');
        }
    })


    /*team*/

    AOS.init({
        once: true,
    });

})(jQuery)
